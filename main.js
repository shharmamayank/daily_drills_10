const fs = require("fs");
const path = require("path")

// // const fs = require("fs");
// // const path = require("path");
// function WriteFile(file) {
//     return new Promise((resolve, reject) => {
//         fs.writeFile(path.join(__dirname, `${file}.json`), JSON.stringify({ 'key': 'Data' }), (err, data) => {
//             if (err) {
//                 new Error("error", err)
//                 reject(err)
//             } else {
//                 console.log("Created");
//                 resolve()
//             }
//         })

//     })
// }
// function DeletFiles(array) {
//     return new Promise((resolve, reject) => {
//         for (let index = 0; index < array.length; index++) {
//             fs.unlink(path.join(__dirname, `${array[index]}.json`), (err, data) => {
//                 if (err) {
//                     err = new Error("error in deleting")
//                 } else {
//                     console.log("deleted");
//                 }
//             })
//         }
//     })
// }
// function CreatedFiles(firstfolder, secondfolder) {
//     let arr = [firstfolder, secondfolder]
//     let filesOne = WriteFile(`${firstfolder}`)
//     let fileTwo = WriteFile(`${secondfolder}`)
//     let deletefolder = new Promise((resolve, reject) => {
//         setTimeout(() => {
//             return DeletFiles(arr)

//         }, 2 * 1000)
//     })
//     return Promise.all([filesOne, fileTwo, deletefolder]).then(() => {
//         console.log("files created");

//     })

// }

// CreatedFiles("file", "filesecond")


// fetch("https://baconipsum.com/api/?type=all-meat&sentences=100&start-with-lorem=1%22")
//     .then((data) => {
//         if (data.ok) {
//             return data.json();
//         }
//     }).then((data) => {
//         fs.writeFile(path.join(__dirname, `lipsum.json`), (JSON.stringify(data)), (err, data) => {
//             if (err) {
//                 new Error("error writeing file", err)
//                 reject(err)
//             } else {
//                 fs.readFile(path.join(__dirname, 'lipsum.json'), (err, data) => {
//                     if (err) {
//                         new Error("reading error")
//                     } else {
//                         console.log("readed the file");
//                         fs.writeFile(path.join(__dirname, `lipsum1.json`), JSON.stringify(data), (err, data) => {
//                             if (err) {
//                                 throw new Error("writing file", err)
//                             } else {
//                                 fs.unlink(path.join(__dirname, `lipsum.json`), (err, data) => {
//                                     if (err) {
//                                         throw new Error("deleting file error")
//                                     } else {
//                                         console.log("deleted the file");
//                                     }
//                                 })
//                             }
//                         })
//                     }
//                 })
//             }
//         })
//     })

function logData(user, activity) {
    return new Promise((resolve, reject) => {
        fs.appendFile(path.join(__dirname, "status.txt"), (`${user} - ${activity}  Time - ${new Date()}\n`), (err, data) => {
            if (err) {
                console.log(err);
                reject()
            } else {
                return resolve()
            }
        })
    })
}
function findLogin(user, id) {
    login(user, id)
        .then((userName) => {
            logData(userName, "Login success")
            getData()
                .then((data) => {
                    let result = data.filter((info) => {
                        return info.id == id
                    });
                    if (result.length !== 0) {
                        logData(user, "GetData success")
                    } else {
                        logData(user, "GetData failure")
                    }
                })

        })
        .catch((err) => {
            console.log(err);
            logData(user, "Login failure")
            getData()
                .then((data) => {
                    let result = data.filter((info) => {
                        return info.id == id
                    });
                    if (result.length !== 0) {
                        logData(user, "GetData success")
                    } else {
                        logData(user, "GetData failure")
                    }
                })
        })
}
function login(user, val) {
    if (val % 2 === 0) {
        return Promise.resolve(user);
    } else {
        return Promise.reject(new Error("User not found"));
    }
}

function getData() {
    return Promise.resolve([
        {
            id: 1,
            name: "Test",
        },
        {
            id: 2,
            name: "Test 2",
        }
    ]);
}